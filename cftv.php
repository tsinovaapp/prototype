<?php
include("header.php");
include("sidebar.php");
include("top.php");
?>

<div class="container">

		<h2 style="display: flex; align-items: center;"><img src="img/cctv.svg" class="img-fluid" style="width: 35px; margin-right: 10px;"> CFTV <span style="color: #dddddd; margin-left: 10px;">Câmeras</span>
		</h2>

	<br/>
	<div class="flex-row" style="justify-content: flex-start;align-items: center; margin-bottom: 10px;">
		<label class="col-form-label" style="color: #757470;font-family: 'Lato',sans-serif">Visão Geral: </label>
		<select class="form-control" name="redes" style="width: 150px;margin-left: 10px; height: 34px;">
			<option value="">Recepção 1</option>
			<option value="14">Recepção 2</option>
			<option value="15">Sala 204</option>
			<option value="16">Sala 102</option>
		</select>
	</div>
	<img src="img/visaogeral-cftv1.png" class="img-fluid">
	<br><br>
	<div style="background: #fff; width: 93%; height: auto; margin: auto; border-radius: 10px; border: 1px solid #dcdcdc; padding: 2%;">
		<table class="table table-bordered table-hover" style="font-size: 14px;">
			<thead>
				<tr>
					<th>Câmera</th>
					<th>IP</th>
					<th>Status</th>
					<th>Gravação</th>
					<th>Qualidade</th>
				</tr>
			</thead>
			<tbody id="myTable">
				<tr">
				<td>1 - Recepção</td>
				<td>10.2.8.7</td>
				<td class="text-success">Online</td>
				<td>Gravando</td>
				<td>720p</td>
			</tr>
			<tr>
				<td>2 - Recepção</td>
				<td>10.2.8.8</td>
				<td class="text-success">Online</td>
				<td>Gravando</td>
				<td>720p</td>
			</tr>
			<tr>
				<td>3 - Recepção</td>
				<td>10.0.5.2</td>
				<td class="text-success">Online</td>
				<td class="text-warning">Não gravando</td>
				<td>720p</td>
			</tr>
			<tr>
				<td>4 - Recepção</td>
				<td>10.2.8.10</td>
				<td class="text-success">Online</td>
				<td>Gravando</td>
				<td>720p</td>
			</tr>
		</tbody>
	</table>
</div>
<br/>


<br/>
<h2>Servidores</h2><hr>
<div class="flex-row" style="justify-content: flex-start;align-items: center; margin-bottom: 10px;">
	<label class="col-form-label" style="color: #757470;font-family: 'Lato',sans-serif">Visão Geral: </label>
	<select class="form-control" name="redes" style="width: 150px;margin-left: 10px; height: 34px;">
		<option value="">Servidor 1</option>
		<option value="14">Servidor 2</option>
	</select>
</div>
<div class="flex-row">
<div style="background: #fff; width: 75%; height: auto; margin: auto; border-radius: 10px; border: 1px solid #dcdcdc; padding: 3%;" class="ml-0">
			<h4>Memória RAM</h4>
			<div id="ram-graphic" style="width: 90%; height: 300px; margin: 0 auto"></div>
			<!--<img src="img/ram-graphic.png" class="img-fluid">-->
			<br/>
			<h4>CPU</h4>
			<div id="cpu-graphic" style="width: 90%; height: 300px; margin: 0 auto"></div>
			<!--<img src="img/cpu-graphic.png" class="img-fluid">-->
			<br/>
		</div>
		<div class="flex-column m-0" style="width: 23%">
			<div class="box2">
				<img src="img/consumo-ram.png" class="img-fluid">
			</div>

			<div class="box2">
				<img src="img/consumo-cpu.png" class="img-fluid">
			</div>


			<div class="box2">
				<img src="img/espaco-servidor.png" class="img-fluid">
			</div>
		</div>
	</div>

	<div style="background: #fff; width: 98%; height: auto; margin: 2% auto; border-radius: 10px; border: 1px solid #dcdcdc; padding: 2%;" class="ml-0">
		<h5>Processos:</h5>
	<img src="img/processos-cftv-kibana.png" class="img-fluid">
	</div>

	<div style="background: #fff; width: 98%; height: auto; margin: 2% auto; border-radius: 10px; border: 1px solid #dcdcdc; padding: 2%;" class="ml-0">
		<h5>Tráfego nos últimos 15 minutos:</h5>
		<div id="trafego-redes">
			<!-- <img src="img/trafego-portas.png" class="img-fluid" style="max-width: 100%;">-->
		</div>
	</div>
</div>


<br/><br/>
<script>
	$(document).ready(function(){
		$("#myInput").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$("#myTable tr").filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});
	});


	Highcharts.chart('cpu-graphic', {
		chart: {
			type: 'area'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: 'Utilizado x Disponível nos últimos 15 minutos'
		},
		xAxis: {
			categories: ['08:00', '08:01', '08:02', '08:03', '08:04', '08:05', '08:06', '08:07', '08:08', '08:09', '08:10', '08:11', '08:12', '08:13'],
			tickmarkPlacement: 'on',
			title: {
				enabled: false
			}
		},
		yAxis: {
			title: {
				text: 'Consumo %'
			},
			labels: {
				formatter: function() {
					return this.value;
				}
			}
		},
		tooltip: {
			split: true,
			valueSuffix: ' %'
		},
		plotOptions: {
			area: {
				stacking: 'normal',
				lineColor: '#666666',
				lineWidth: 1,
				marker: {
					lineWidth: 1,
					lineColor: '#666666'
				}
			},
			series: {
				lineColor: '#fff'
			}
		},
		series: [{
			name: 'Disponível',
			color: '#9ED17F',
			marker: {
				symbol: "circle",
				radius: 0
			},
			data: [90, 85, 80, 95, 70, 55, 40, 30, 45, 60, 50, 20, 25, 45]
		}, {
			name: 'Utilizada',
			color: '#f24848',
			marker: {
				symbol: "circle",
				radius: 0
			},
			data: [10, 15, 20, 5, 30, 45, 60, 70, 55, 40, 50, 80, 75, 55]
		}]
	});

	Highcharts.chart('ram-graphic', {
		chart: {
			type: 'area'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: 'Disponível x Cache x Utilizada nos últimos 15 minutos'
		},
		xAxis: {
			categories: ['08:00', '08:01', '08:02', '08:03', '08:04', '08:05', '08:06', '08:07', '08:08', '08:09', '08:10', '08:11', '08:12', '08:13'],
			tickmarkPlacement: 'on',
			title: {
				enabled: false
			}
		},
		yAxis: {
			title: {
				text: 'Megabits (mb)'
			},
			labels: {
				formatter: function() {
					return this.value;
				}
			}
		},
		tooltip: {
			split: true,
			valueSuffix: ' Megabits (mb)'
		},
		plotOptions: {
			area: {
				stacking: 'normal',
				lineColor: '#666666',
				lineWidth: 1,
				marker: {
					lineWidth: 1,
					lineColor: '#666666'
				}
			},
			series: {
				lineColor: '#fff'
			}
		},
		series: [{
			name: 'Disponível',
			color: '#9ED17F',
			marker: {
				symbol: "circle",
				radius: 0
			},
			data: [3300, 3000, 3500, 4000, 3900, 2600, 3900, 1500, 300, 300, 2000, 1800, 1800, 1800]
		},

		{
			name: 'Cache',
			color: '#9fbff2',
			marker: {
				symbol: "circle",
				radius: 0
			},
			data: [200, 200, 500, 200, 200, 1200, 300, 700, 2000, 2000, 800, 1000, 800, 1000]
		},
		{
			name: 'Utilizada',
			color: '#f24848',
			marker: {
				symbol: "circle",
				radius: 0
			},
			data: [1500, 1800, 1000, 800, 900, 1200, 800, 2800, 2700, 2700, 2200, 2200, 2400, 2200]
		}]
	});


</script>

<?php
include("footer.php");
?>
