
<div class="nav-side-menu">
	<div class="brand">
		<a href="dashboard.php"><img src="img/cliente1.png" class="img-fluid" style="height: 15%"></a>

	</div>
	<ul id="perfil-side">
		<li  data-toggle="collapse" data-target="#perfil">
			<a href="#" class="nav-item"><img src="img/jane.png" style="width: 30px; margin-right: 7px; border-radius: 50%">
				Jane Doe
				<ion-icon name="arrow-dropright"></ion-icon>
			</a>
		</li>
		<ul class="sub-menu collapse" id="perfil">
			<li class="active"><a href="#">Perfil</a></li>
			<li><a href="#">Configurações</a></li>
			<li><a href="index.php">Logout</a></li>
		</ul>

	</ul>
	<i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

	<div class="menu-list">

		<ul id="menu-content" class="menu-content collapse out">
			<span style="color: white; text-align: right; padding: 2px 8px 2px 8px; font-family:'Open Sans',sans-serif; font-size: 1rem; letter-spacing: .1em;">NAVEGAÇÃO</span>
			<li>
				<a href="dashboard.php" class="nav-item">
					<ion-icon name="globe"></ion-icon> Dashboard
				</a>
			</li>

			<li  data-toggle="collapse" data-target="#sistemas" class="collapsed">
				<a href="#" class="nav-item"><ion-icon name="pie"></ion-icon> Sistemas <ion-icon name="arrow-dropright"></ion-icon></a>
			</li>
			<ul class="sub-menu collapse" id="sistemas">
				<li><a href="sca.php">SCA</a></li>
				<li><a href="cftv.php">CFTV</a></li>
				<li><a href="redes.php">Redes</a></li>
				<li><a href="nobreak.php">Nobreak</a></li>
				<li><a href="cabeamento.php">Cabeamento</a></li>
				<li><a href="enfermaria.php">Enfermeira</a></li>
			</ul>


			<li data-toggle="collapse" data-target="#modulo" class="collapsed">
				<a href="analise.php" class="nav-item"><ion-icon name="analytics"></ion-icon> Módulo de Análise </a>
			</li>



			<li data-toggle="collapse" data-target="#machine" class="collapsed">
				<a href="machinelearning.php" class="nav-item"><ion-icon name="desktop"></ion-icon> Machine Learning </a>
			</li>


			<li data-toggle="collapse" data-target="#machine" class="collapsed">
				<a href="relatorios.php" class="nav-item">
					<ion-icon name="clipboard"></ion-icon> Relatórios
				</a>
			</li>

			<li data-toggle="collapse" data-target="#alarmes" class="collapsed">
				<a href="alarmes.php" class="nav-item"><ion-icon name="alarm"></ion-icon> Alarmes e Logs </a>
			</li>
			
			<li data-toggle="collapse" data-target="#configuracoes" class="collapsed">
				<a href="configuracoes.php" class="nav-item"><ion-icon name="settings"></ion-icon> Configurações </a>
			</li>

		</ul>
	</div>
</div>
