<?php
include("header.php");
include("sidebar.php");
?>

<div class="container">
	<div class="flex-row" style="align-content: space-between; align-items: center;">
		<!-- Dashboard Text -->
		<h2 style="font-family: 'Lato', sans-serif; color: #757470; font-weight: bold; display:flex; align-items: center;">
			<ion-icon name="globe"></ion-icon>
			Dashboard
		</h2>
		<!-- Cubee logo -->
		<img src="img/logo1.png" class="img-top">
	</div>

	<br/>
	<!-- SCA -->
	<a href="sca.php" style="text-decoration: none;">
		<div class="flex-row">

			<span style="background: #d6e5ff; height: auto; width: 1%; "></span>

			<div class="flex-column" style="width: 100%; margin-left: 2%;">

				<div class="flex-row" style="align-content: center; align-items: center; max-width: 97%;">
					<h2> SCA <span style="color: #dddddd;">Catracas</span></h2>

					<span style="justify-content: flex-end; align-items: center; display: flex;text-transform: uppercase;"><i class="material-icons" style="color: #28a745">fiber_manual_record</i> <span class="text-success">Sistema funcionando</span></span>
				</div>

				<div class="flex-row">

					<div class="box-sistemas flex-row" style="align-items: center; justify-content: space-between;">
						<div class="flex-column">
							<div class="flex-row" style="align-items: center;">
								<span style="font-size: 25px; margin-right: 5px;">8</span> Catracas
							</div>
							<div class="flex-row" style="align-items: center;">
								<span style="font-size: 25px; margin-right: 5px;">2</span> Servidores
							</div>
						</div>
						<img src="img/catraca-cinza.svg" style="width: 30%; align-self: flex-end; color:#ddd;">
					</div>

					<div class="box-sistemas" style="justify-content: center;">
						<h3 class="text-success" style="font-size: 50px; margin:0 10px 0 0;">0</h3> tentativas de arrombamento
					</div>

					<div class="box-sistemas">
						<h3 class="text-danger" style="font-size: 50px; margin:0 10px 0 0;">1</h3> acesso vencido
					</div>

					<div class="box-sistemas">
						<i class="material-icons" style="font-size: 50px; margin:0 10px 0 0;">notifications_none</i>
						sem novas notificações
					</div>
				</div>
			</div>
		</div>
	</a>
	<br/>
	<!-- CFTV -->
	<a href="cftv.php" style="text-decoration: none;">
		<div class="flex-row">
			<span style="background: #dfd5ee; height: auto; width: 1%;"></span>

			<div class="flex-column" style="width: 100%; margin-left: 2%">
				<div class="flex-row" style="align-content: center; align-items: center; max-width: 97%;">
					<h2> CFTV <span style="color: #dddddd;">Câmeras</span></h2>

					<span style="justify-content: flex-end; align-items: center; display: flex;text-transform: uppercase;"><i class="material-icons" style="color: #ffc107">fiber_manual_record</i> <span class="text-warning">OPERANDO COM PROBLEMAS</span></span>
				</div>

				<div class="flex-row">

					<div class="box-sistemas flex-row" style=" justify-content: space-between;">

						<div class="flex-column">
							<div class="flex-row" style="align-items: center;">
								<span style="font-size: 25px; margin-right: 5px;">6</span> Câmeras
							</div>
							<div class="flex-row" style="align-items: center;">
								<span style="font-size: 25px; margin-right: 5px;">2</span> Servidores
							</div>
						</div>

						<img src="img/cctv-cinza.svg" style="width: 39%; align-self: flex-end; top: 0; fill: #ddd;">
					</div>

					<div class="box-sistemas">
						<h3 class="text-success" style="font-size: 50px; margin:0 10px 0 0;">6</h3> câmeras online
					</div>

					<div class="box-sistemas">
						<h3 class="text-warning" style="font-size: 50px; margin:0 10px 0 0;">5</h3> câmeras gravando
					</div>

					<div class="box-sistemas">
						<i class="material-icons" style="font-size: 50px; margin:0 10px 0 0;">notifications_active</i>
						IP 192.8.0.5 não está gravando
					</div>
				</div>
			</div>

		</div>
	</a>
	<br/>
	<!-- REDES -->
	<a href="redes.php" style="text-decoration: none;">
		<div class="flex-row">
			<span style="background: #fddfc5; height: auto; width: 1%;"></span>

			<div class="flex-column" style="width: 100%; margin-left: 2%">
				<div class="flex-row" style="align-content: center; align-items: center; max-width: 97%;">
					<h2> Redes <span style="color: #dddddd;">Switchs e Roteadores</span></h2>

					<span style="justify-content: flex-end; align-items: center; display: flex;text-transform: uppercase;"><i class="material-icons" style="color: #28a745">fiber_manual_record</i> <span class="text-success">Sistema funcionando</span></span>
				</div>

				<div class="flex-row">

					<div class="box-sistemas flex-row" style=" justify-content: space-between;">

						<div class="flex-column">
							<div class="flex-row" style="align-items: center;">
								<span style="font-size: 25px; margin-right: 5px;">1</span> Switch
							</div>
							<div class="flex-row" style="align-items: center;">
								<span style="font-size: 25px; margin-right: 5px;">2</span> Roteadores
							</div>
						</div>

						<img src="img/router-cinza.svg" style="width: 35%; align-self: flex-end;">
					</div>

					<div class="box-sistemas">
						<h3 class="text-success" style="font-size: 50px; margin:0 10px 0 0;">0</h3> CPU com consumo acima de 80%
					</div>

					<div class="box-sistemas">
						<h3 class="text-success" style="font-size: 50px; margin:0 10px 0 0;">0</h3> excesso de consumo de banda
					</div>

					<div class="box-sistemas">
						<i class="material-icons" style="font-size: 50px; margin:0 10px 0 0;">notifications_none</i>
						sem novas notificações
					</div>
				</div>
			</div>

		</div>
	</a>
	<br/>

	<!-- NOBREAK -->
	<a href="nobreak.php" style="text-decoration: none;">
		<div class="flex-row">
			<span style="background: #f9fec8; height: auto; width: 1%;"></span>

			<div class="flex-column" style="width: 100%; margin-left: 2%">

				<div class="flex-row" style="align-content: center; align-items: center; max-width: 97%;">
					<h2> Nobreak <span style="color: #dddddd;"> Bateria</span></h2>

					<span style="justify-content: flex-end; align-items: center; display: flex;text-transform: uppercase;"><i class="material-icons" style="color: #28a745">fiber_manual_record</i> <span class="text-success">Sistema funcionando</span></span>
				</div>

				<div class="flex-row">

					<div class="box-sistemas flex-row" style=" justify-content: space-between;">

						<span><span style="font-size: 25px">2</span> NOBREAKS</span><br/>

						<img src="img/bateria-cinza.svg" style="width: 33%; align-self: flex-end;">
					</div>

					<div class="box-sistemas">
						<h3 class="text-success" style="font-size: 50px; margin:0 10px 0 0;">0</h3>   nobreak com bateria abaixo de 30%
					</div>

					<div class="box-sistemas">
						<h3 class="text-success" style="font-size: 50px; margin:0 10px 0 0;">65</h3> minutos de autonomia
					</div>

					<div class="box-sistemas">
						<i class="material-icons" style="font-size: 50px; margin:0 10px 0 0;">notifications_none</i>
						sem novas notificações
					</div>
				</div>
			</div>
		</div>
	</a>
	<br/>
	<!-- CABEAMENTO -->
	<a href="cabeamento.php" style="text-decoration: none;">
		<div class="flex-row">
			<span style="background: #cefdd3; height: auto; width: 1%;"></span>

			<div class="flex-column" style="width: 100%; margin-left: 2%">

				<div class="flex-row" style="align-content: center; align-items: center; max-width: 97%;">
					<h2> Cabeamento <span style="color: #dddddd;">Dispositivos conectados</span></h2>

					<span style="justify-content: flex-end; align-items: center; display: flex;text-transform: uppercase;"><i class="material-icons" style="color: #ffc107">fiber_manual_record</i> <span class="text-warning">OPERANDO COM PROBLEMAS</span></span>
				</div>


				<div class="flex-row">

					<div class="box-sistemas flex-row" style=" justify-content: space-between;">

						<span><span style="font-size: 25px">15</span> Dispositivos Conectados</span><br/>

						<img src="img/ethernet-cinza.svg" style="width: 25%; align-self: flex-end;">
					</div>

					<div class="box-sistemas">
						<h3 class="text-danger" style="font-size: 50px; margin:0 10px 0 0;">1</h3>   dispositivo não autorizado
					</div>


					<div class="box-sistemas">
						<i class="material-icons" style="font-size: 50px; margin:0 10px 0 0;">notifications_active</i>
						VERIFICAR IP NÃO AUTORIZADO
					</div>
				</div>
			</div>
		</div>
	</a>
	<br/>

	<!-- CHAMADO ENF -->
	<a href="enfermaria.php" style="text-decoration: none;">
		<div class="flex-row">
			<span style="background: #fde6f0; height: auto; width: 1%;"></span>

			<div class="flex-column" style="width: 100%; margin-left: 2%">

				<div class="flex-row" style="align-content: center; align-items: center; max-width: 97%;">
					<h2> Chamado <span style="color: #dddddd;">Enfermaria</span></h2>

					<span style="justify-content: flex-end; align-items: center; display: flex;text-transform: uppercase;"><i class="material-icons" style="color: #28a745">fiber_manual_record</i> <span class="text-success">Sistema funcionando</span></span>
				</div>


				<div class="flex-row">

					<div class="box-sistemas flex-row" style=" justify-content: space-between;">

						<span><span style="font-size: 25px">7</span> APARELHOS CONECTADOS</span><br/>

						<img src="img/estetoscopio-cinza.svg" style="width: 25%; align-self: flex-end;">
					</div>

					<div class="box-sistemas">
						<h3 class="text-success" style="font-size: 50px; margin:0 10px 0 0;">7</h3>   APARELHOS FUNCIONANDO
					</div>


					<div class="box-sistemas">
						<i class="material-icons" style="font-size: 50px; margin:0 10px 0 0;">notifications_none</i>
						SEM NOVAS NOTIFICAÇÕES
					</div>
				</div>
			</div>
		</div>
	</a>
	<br/>

</div>
<?php
include("footer.php");
?>