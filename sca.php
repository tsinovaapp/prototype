<?php
include("header.php");
include("sidebar.php");
include("top.php");
?>

<div class="container">

		<h2 style="display: flex; align-items: center;"><img src="img/catraca.svg" class="img-fluid" style="width: 30px; margin-right: 10px"> SCA <span style="color: #dddddd; margin-left: 10px;">Catracas</span>
		</h2>
	
	
	<br/>
	<div class="flex-row" style="justify-content: flex-start;align-items: center; margin-bottom: 10px;">
		<label class="col-form-label" style="color: #757470;font-family: 'Lato',sans-serif">Visão Geral: </label>
		<select class="form-control" id="catracas" style="width: 150px;margin-left: 10px; height: 34px;">
			<option value="0"> Todas</option>
			<option value="c1">Catraca 1</option>
			<option value="c2">Catraca 2</option>
			<option value="c3">Catraca 3</option>
			<option value="c4">Catraca 4</option>
		</select>
	</div>
	<br/>
	<!-- VISAO GERAL ROW-->
	<div class="container ml-0" id="visaogeral">
		<div class="row">
			<div class="col-3">
				<div class="cx">
					<h1 style="color: #2d2a32;">8</h1>
					<span><span style="font-size: 30px; color: #2d2a32;">Catracas</span> </br><span style="font-size: 20px;">registradas</span></span>
				</div>
			</div>
			<div class="col-3">
				<div class="cx">
					<h1 class="text-success">8</h1>
					<span><span style="font-size: 30px; color: #2d2a32;">Catracas</span> </br>
					<span class="text-success" style="font-size: 20px;">Online</span></span>
				</div>
			</div>
			<div class="col-3">
				<div class="cx">
					<h1 class="text-danger">1</h1>
					<span><span style="font-size: 30px; color: #2d2a32;">Acesso</span> </br>
					<span class="text-danger" style="font-size: 20px;">Vencido</span>
					<span class="text-muted">(Catraca 1)</span>
				</span>
			</div>
		</div>
		<div class="col-3">
			<div class="cx">
				<h1 class="text-success">0</h1>
				<span><span style="font-size: 30px; color: #2d2a32;">Tentativas</span> </br>
				<span class="text-success" style="font-size: 20px;">de Arrombamento</span></span>
			</div>
		</div>
	</div>
</div>

<!-- CATRACAS ROW-->
<div class="container ml-0">
	<div id="catracas-row" style="display: none;">
		<h2>Catraca <span class="exibidor">1</span></h2>
		<hr>

		<div class="row">
			<div class="col-3">
				<div class="cx">
					<span style="font-size: 26px;color: #28a745; font-weight: bolder; text-align: justify;"><span class="exibidor">Operando</span></span>
					<img src="img/catraca-cinza.svg" style="max-width: 20%; margin-left: 50px; ">
				</div>
			</div>
			<div class="col-6">
				<div class="cx">
					<div class="col-6 flex-row">
						<h1 class="text-danger"><span class="exibidor">0</span></h1>
						<span><span style="font-size: 30px; color: #2d2a32;">Acesso</span> </br>
						<span class="text-danger" style="font-size: 20px;">Vencido</span></span>
					</div>

					<div class="col-6" style="text-align: justify;">
						<span data-toggle="collapse" data-target="#acesso"><img src="img/jane.png" style="border-radius: 50%; max-width: 20px; margin-right: 5px; align-content: center;"><b>Jane Doe</b></span><br>

						<span class="collapse" id="acesso">
							<p style="font-size: 12px;">
								Acesso venceu em: <b>15/10/2018</b><br>
								Motivo: <b>Data limite para renovação de credencial expirada.</b>
							</p>
						</span>
					</div>

				</div>
			</div>
			<div class="col-3">
				<div class="cx">
					<h1 class="text-success"><span class="exibidor">0</span></h1>
					<span><span style="font-size: 30px; color: #2d2a32;">Tentativas</span> </br>
					<span class="text-success" style="font-size: 20px;">de Arrombamento</span></span>
				</div>
			</div>
		</div>

		<div class="col-12 cx flex-column m-0">
			<h5 style="text-align: left;">ACESSOS NA CATRACA <span class="exibidor">1</span>:</h5><br>

			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col">ID</th>
						<th scope="col">Nome</th>
						<th scope="col">Setor</th>
						<th scope="col">Vencimento</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">1</th>
						<td>Mark Jacobs</td>
						<td>Recepção</td>
						<td>10-01-2019</td>
					</tr>
					<tr>
						<th scope="row">2</th>
						<td>Louis Otto</td>
						<td>Enfermagem</td>
						<td>12-05-2019</td>
					</tr>
					<tr class="text-danger">
						<th scope="row">3</th>
						<td>Jane Doe</td>
						<td>Recepção</td>
						<td>15-10-2018</td>
					</tr>
					<tr>
						<th scope="row">4</th>
						<td>Leo Barbosa</td>
						<td>TI</td>
						<td>12-08-2020</td>
					</tr>
				</tbody>
			</table>

		</div>
	</div>

	<br/>
	<h2>Servidores</h2><hr>
	<div class="flex-row" style="justify-content: flex-start;align-items: center; margin-bottom: 10px;">
		<label class="col-form-label" style="color: #757470;font-family: 'Lato',sans-serif">Visão Geral: </label>
		<select class="form-control" name="redes" style="width: 150px;margin-left: 10px; height: 34px;">
			<option value="">Servidor 1</option>
			<option value="14">Servidor 2</option>
		</select>
	</div>
	<div class="flex-row">
		<div style="background: #fff; width: 75%; height: auto; margin: auto; border-radius: 10px; border: 1px solid #dcdcdc; padding: 3%;" class="ml-0">
			<h4>Memória RAM</h4>
			<div id="ram-graphic" style="width: 90%; height: 300px; margin: 0 auto"></div>
			<!--<img src="img/ram-graphic.png" class="img-fluid">-->
			<br/>
			<h4>CPU</h4>
			<div id="cpu-graphic" style="width: 90%; height: 300px; margin: 0 auto"></div>
			<!--<img src="img/cpu-graphic.png" class="img-fluid">-->
			<br/>
		</div>
		<div class="flex-column m-0" style="width: 23%">
			<div class="box2">
				<img src="img/consumo-ram.png" class="img-fluid">
			</div>

			<div class="box2">
				<img src="img/consumo-cpu.png" class="img-fluid">
			</div>


			<div class="box2">
				<img src="img/espaco-servidor.png" class="img-fluid">
			</div>
		</div>
	</div>

	<div style="background: #fff; width: 98%; height: auto; margin: 2% auto; border-radius: 10px; border: 1px solid #dcdcdc; padding: 2%;" class="ml-0">
		<h5>Processos:</h5>
		<img src="img/processos-cftv-kibana.png" class="img-fluid">
	</div>

	<div style="background: #fff; width: 98%; height: auto; margin: 2% auto; border-radius: 10px; border: 1px solid #dcdcdc; padding: 2%;" class="ml-0">
		<h5>Tráfego nos últimos 15 minutos:</h5>
		<div id="trafego-redes">
			<!-- <img src="img/trafego-portas.png" class="img-fluid" style="max-width: 100%;">-->
		</div>
	</div>
</div>


<br/><br/>
<script>
	$(document).ready(function(){
		$("#myInput").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$("#myTable tr").filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});
	});


	Highcharts.chart('cpu-graphic', {
		chart: {
			type: 'area'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: 'Utilizado x Disponível nos últimos 15 minutos'
		},
		xAxis: {
			categories: ['08:00', '08:01', '08:02', '08:03', '08:04', '08:05', '08:06', '08:07', '08:08', '08:09', '08:10', '08:11', '08:12', '08:13'],
			tickmarkPlacement: 'on',
			title: {
				enabled: false
			}
		},
		yAxis: {
			title: {
				text: 'Consumo %'
			},
			labels: {
				formatter: function() {
					return this.value;
				}
			}
		},
		tooltip: {
			split: true,
			valueSuffix: ' %'
		},
		plotOptions: {
			area: {
				stacking: 'normal',
				lineColor: '#666666',
				lineWidth: 1,
				marker: {
					lineWidth: 1,
					lineColor: '#666666'
				}
			},
			series: {
				lineColor: '#fff'
			}
		},
		series: [{
			name: 'Disponível',
			color: '#9ED17F',
			marker: {
				symbol: "circle",
				radius: 0
			},
			data: [90, 85, 80, 95, 70, 55, 40, 30, 45, 60, 50, 20, 25, 45]
		}, {
			name: 'Utilizada',
			color: '#f24848',
			marker: {
				symbol: "circle",
				radius: 0
			},
			data: [10, 15, 20, 5, 30, 45, 60, 70, 55, 40, 50, 80, 75, 55]
		}]
	});

	Highcharts.chart('ram-graphic', {
		chart: {
			type: 'area'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: 'Disponível x Cache x Utilizada nos últimos 15 minutos'
		},
		xAxis: {
			categories: ['08:00', '08:01', '08:02', '08:03', '08:04', '08:05', '08:06', '08:07', '08:08', '08:09', '08:10', '08:11', '08:12', '08:13'],
			tickmarkPlacement: 'on',
			title: {
				enabled: false
			}
		},
		yAxis: {
			title: {
				text: 'Megabits (mb)'
			},
			labels: {
				formatter: function() {
					return this.value;
				}
			}
		},
		tooltip: {
			split: true,
			valueSuffix: ' Megabits (mb)'
		},
		plotOptions: {
			area: {
				stacking: 'normal',
				lineColor: '#666666',
				lineWidth: 1,
				marker: {
					lineWidth: 1,
					lineColor: '#666666'
				}
			},
			series: {
				lineColor: '#fff'
			}
		},
		series: [{
			name: 'Disponível',
			color: '#9ED17F',
			marker: {
				symbol: "circle",
				radius: 0
			},
			data: [3300, 3000, 3500, 4000, 3900, 2600, 3900, 1500, 300, 300, 2000, 1800, 1800, 1800]
		},

		{
			name: 'Cache',
			color: '#9fbff2',
			marker: {
				symbol: "circle",
				radius: 0
			},
			data: [200, 200, 500, 200, 200, 1200, 300, 700, 2000, 2000, 800, 1000, 800, 1000]
		},
		{
			name: 'Utilizada',
			color: '#f24848',
			marker: {
				symbol: "circle",
				radius: 0
			},
			data: [1500, 1800, 1000, 800, 900, 1200, 800, 2800, 2700, 2700, 2200, 2200, 2400, 2200]
		}]
	});
</script>



<script type="text/javascript">

	$( document ).ready(function() {


		var data_set_1 = [1, 'Operando', 1, 0,1];
		var data_set_2 = [2,'Operando',0, 0,2];
		var data_set_3 = [3,'Operando',0, 0,3];
		var data_set_4 = [4,'Operando',0, 0,4];

		$('#catracas').change(function() {
			if ($('#catracas').val()=='0'){
				$('#catracas-row').css('display', 'none');
				$('#visaogeral').css('display', 'inherit');
			} else {
				$('#catracas-row').css('display', 'inherit');
				$('#visaogeral').css('display', 'none');
			}


			var id = $('#catracas').val();
			switch(id){
				case 'c1':
				$('.exibidor').each(function(key, value){
					$(value).html(data_set_1[key]);
				});
				break;
				case 'c2':
				$('.exibidor').each(function(key, value){
					$(value).html(data_set_2[key]);
				});
				break;
				case 'c3':
				$('.exibidor').each(function(key, value){
					$(value).html(data_set_3[key]);
				});
				break;
				case 'c4':
				$('.exibidor').each(function(key, value){
					$(value).html(data_set_4[key]);
				});
				break
			}
		});
	});
	
</script>


<?php
include("footer.php");
?>
