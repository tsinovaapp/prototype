<?php
include("header.php");
?>
<div class="login-clean">
	<form method="post" action="javascript:()">
		<h2 class="sr-only">Login Form</h2>

		<div class="illustration">
			<img src="img/logo1.jpg" id="logo" style="max-width: 200px" class="fluid" />
			<i class="icon ion-ios-lock icon-logo"></i>
		</div>

		<div class="form-group"><input type="email" name="email" placeholder="Email" class="form-control" /></div>

		<div class="form-group"><input type="password" name="password" placeholder="Senha" class="form-control" /></div>
		<div class="form-group"><a href="loading.php" style="text-decoration: none;" class="btn btn-primary btn-block">Entrar</a></div>
		<a href="redes.php" class="forgot">Esqueceu seu e-mail ou senha?</a>

	</form>
	<div style="align-items: center; text-align: center; max-width: 400px; margin: 0 auto; color: #c4c4c4">
	<hr style="background: #ddd;"/>
		© TSInova - Todos os direitos reservados - 2018
	</div>
</div>


<?php
include("footer.php");
?>