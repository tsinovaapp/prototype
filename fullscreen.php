<?php
include("header.php");
include("top.php");
?>

<div class="container" style="width: 99% !important; margin: auto !important;">

		<h2 style="display: flex; align-items: center;"><img src="img/router.svg" class="img-fluid" style="width: 35px; margin-right: 10px;"> Redes
		</h2>

	
	<br/>
	<div class="flex-row" style="justify-content: flex-start;align-items: center; margin-bottom: 10px;">
		<label class="col-form-label" style="color: #757470;font-family: 'Lato',sans-serif">Visão Geral: </label>
		<select class="form-control" name="redes" style="width: 150px;margin-left: 10px; height: 34px;">
			<option value="">Roteador 1</option>
			<option value="14">Switch</option>
			<option value="15">Roteador 2</option>
		</select>
	</div>
	<br/>
	<div class="flex-row">

		<div class="box1">

			<h4>Memória RAM</h4>
			<div id="ram-graphic" style="width: 100%; height: 350px; margin: 0 auto"></div>
			<!--<img src="img/ram-graphic.png" class="img-fluid">-->
			<br/>
			<h4>CPU</h4>
			<div id="cpu-graphic" style="width: 100%; height: 350px; margin: 0 auto"></div>
			<!--<img src="img/cpu-graphic.png" class="img-fluid">-->
			<br/>
			<h4>Tráfego na Rede</h4>
			<br/>
			<div class="flex-row" style="align-items: center;">
				<i class="material-icons"style="color:#2be76d; font-size: 8rem;">
					keyboard_arrow_up
				</i>
				<img src="img/trafego-entrada2.png" class="img-fluid" style="width: 220px">
				<img src="img/espaco.png" class="img-fluid">
				<i class="material-icons"style="color:#f82b2b; font-size: 8rem;" >
					keyboard_arrow_down
				</i>
				<img src="img/trafego-saida2.png" class="img-fluid"  style="width: 220px">
			</div>
			<br/><br/>

		</div>

		<div class="flex-column">

			<div class="box2">
				<h5>RAM</h5>
				<img src="img/ram1.jpg" class="img-fluid">
				45.5% utilizada de 2GB
			</div>

			<div class="box2">
				<h5>CPU</h5>
				<img src="img/cpu1.jpg" class="img-fluid">
			</div>

			<div class="box2">
				<h5><img src="img/termometro-icon.png" width="30px"> Temperatura</h5>
				<img src="img/Temperatura.png" class="img-fluid">
			</div>

			<div class="box2">
				<h5> Mais informações</h5><br>
				<p style="text-align:left"><b>Localização:</b> 10.0.8.1 - Recepção
					<br><br><b>Uptime:</b> 00:01:30</p>
				</div>

			</div>
		</div>
		<div class="flex-row">
			<div id="conteudo">
				<h4>Portas:</h4>
				<table class="table table-bordered table-striped" style="font-size: 14px;">
					<thead>
						<tr>
							<th>Porta</th>
							<th>Status</th>
							<th>VLAN</th>
							<th colspan="2">Tráfego</th>
							<th>Consumo</th>
						</tr>
					</thead>
					<tbody id="myTable">
						<tr>
							<td>Porta 1</td>
							<td class="text-success">UP</td>
							<td>Privada AB</td>
							<td> <ion-icon name="arrow-dropdown-circle" style="color:#2be76d; font-size: 1.3rem;"></ion-icon> 250Kbps  </td>

							<td><ion-icon name="arrow-dropup-circle" style="color:#f82b2b; font-size: 1.3rem;"></ion-icon> 100Kbps</td>
							<td>3,5w</td>
						</tr>
						<tr>
							<td>Porta 2</td>
							<td class="text-danger">Down</td>
							<td>LAN A</td>
							<td> <ion-icon name="arrow-dropdown-circle" style="color:#2be76d; font-size: 1.3rem;"></ion-icon> 0Kbps  </td>

							<td><ion-icon name="arrow-dropup-circle" style="color:#f82b2b; font-size: 1.3rem;"></ion-icon> 0Kbps</td>
							<td>0,5w</td>
						</tr>
						<tr>
							<td>Porta 3</td>
							<td class="text-success">UP</td>
							<td>LAN B</td>
							<td> <ion-icon name="arrow-dropdown-circle" style="color:#2be76d; font-size: 1.3rem;"></ion-icon> 56Kbps  </td>

							<td><ion-icon name="arrow-dropup-circle" style="color:#f82b2b; font-size: 1.3rem;"></ion-icon> 100Kbps</td>
							<td>1,2w</td>
						</tr>
						<tr>
							<td>Porta 4</td>
							<td class="text-success">UP</td>
							<td>Pública IDF</td>
							<td> <ion-icon name="arrow-dropdown-circle" style="color:#2be76d; font-size: 1.3rem;"></ion-icon> 256Kbps  </td>

							<td><ion-icon name="arrow-dropup-circle" style="color:#f82b2b; font-size: 1.3rem;"></ion-icon> 10Kbps</td>
							<td>1,5w</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<br/><br/>
	<script>
		$(document).ready(function(){
			$("#myInput").on("keyup", function() {
				var value = $(this).val().toLowerCase();
				$("#myTable tr").filter(function() {
					$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
				});
			});
		});


		Highcharts.chart('cpu-graphic', {
			chart: {
				type: 'area'
			},
			title: {
				text: ''
			},
			subtitle: {
				text: 'Utilizado x Disponível nos últimos 25 minutos'
			},
			xAxis: {
				type: 'datetime',
				dateTimeLabelFormats: { // don't display the dummy year
						month: '%e. %b',
						year: '%b'
				},
				title: {
					enabled: false
				}
			},
			yAxis: {
				title: {
					text: 'Consumo %'
				},
				labels: {
					formatter: function() {
						return this.value;
					}
				}
			},
			tooltip: {
				split: true,
				valueSuffix: ' %'
			},
			plotOptions: {
				area: {
					stacking: 'normal',
					lineColor: '#666666',
					lineWidth: 1,
					marker: {
						lineWidth: 1,
						lineColor: '#666666'
					}
				},
				series: {
					lineColor: '#fff'
				}
			},
			series: [{
				name: 'Disponível',
				color: '#9ED17F',
				marker: {
					symbol: "circle",
					radius: 0
				},
				data: [[Date.UTC(2018, 10, 25, 0, 0, 0),70],
							 [Date.UTC(2018, 10, 25, 0, 1, 0),55],
							 [Date.UTC(2018, 10, 25, 0, 2, 0),65],
							 [Date.UTC(2018, 10, 25, 0, 3, 0),75],
							 [Date.UTC(2018, 10, 25, 0, 4, 0),70],
							 [Date.UTC(2018, 10, 25, 0, 5, 0),55],
							 [Date.UTC(2018, 10, 25, 0, 6, 0),40],
							 [Date.UTC(2018, 10, 25, 0, 7, 0),50],
							 [Date.UTC(2018, 10, 25, 0, 8, 0),45],
							 [Date.UTC(2018, 10, 25, 0, 9, 0),60],
							 [Date.UTC(2018, 10, 25, 0, 10, 0),50],
							 [Date.UTC(2018, 10, 25, 0, 11, 0),20],
							 [Date.UTC(2018, 10, 25, 0, 12, 0),25],
							 [Date.UTC(2018, 10, 25, 0, 13, 0),45],
							 [Date.UTC(2018, 10, 25, 0, 14, 0),70],
			 				 [Date.UTC(2018, 10, 25, 0, 15, 0),55],
			 				 [Date.UTC(2018, 10, 25, 0, 16, 0),65],
			 				 [Date.UTC(2018, 10, 25, 0, 17, 0),75],
			 				 [Date.UTC(2018, 10, 25, 0, 18, 0),70],
			 				 [Date.UTC(2018, 10, 25, 0, 19, 0),55],
			 				 [Date.UTC(2018, 10, 25, 0, 20, 0),40],
			 				 [Date.UTC(2018, 10, 25, 0, 21, 0),50],
			 				 [Date.UTC(2018, 10, 25, 0, 22, 0),45],
			 				 [Date.UTC(2018, 10, 25, 0, 23, 0),60],
			 				 [Date.UTC(2018, 10, 25, 0, 24, 0),50],
			 				 [Date.UTC(2018, 10, 25, 0, 25, 0),20]]
			}, {
				name: 'Utilizada',
				color: '#f24848',
				marker: {
					symbol: "circle",
					radius: 0
				},
				data: [[Date.UTC(2018, 10, 25, 0, 0, 0),30],
							 [Date.UTC(2018, 10, 25, 0, 1, 0),45],
							 [Date.UTC(2018, 10, 25, 0, 2, 0),35],
							 [Date.UTC(2018, 10, 25, 0, 3, 0),25],
							 [Date.UTC(2018, 10, 25, 0, 4, 0),30],
							 [Date.UTC(2018, 10, 25, 0, 5, 0),45],
							 [Date.UTC(2018, 10, 25, 0, 6, 0),60],
							 [Date.UTC(2018, 10, 25, 0, 7, 0),50],
							 [Date.UTC(2018, 10, 25, 0, 8, 0),55],
							 [Date.UTC(2018, 10, 25, 0, 9, 0),40],
							 [Date.UTC(2018, 10, 25, 0, 10, 0),50],
							 [Date.UTC(2018, 10, 25, 0, 11, 0),80],
							 [Date.UTC(2018, 10, 25, 0, 12, 0),75],
							 [Date.UTC(2018, 10, 25, 0, 13, 0),55],
							 [Date.UTC(2018, 10, 25, 0, 14, 0),30],
			 				 [Date.UTC(2018, 10, 25, 0, 15, 0),45],
			 				 [Date.UTC(2018, 10, 25, 0, 16, 0),35],
			 				 [Date.UTC(2018, 10, 25, 0, 17, 0),25],
			 				 [Date.UTC(2018, 10, 25, 0, 18, 0),30],
			 				 [Date.UTC(2018, 10, 25, 0, 19, 0),45],
			 				 [Date.UTC(2018, 10, 25, 0, 20, 0),60],
			 				 [Date.UTC(2018, 10, 25, 0, 21, 0),50],
			 				 [Date.UTC(2018, 10, 25, 0, 22, 0),55],
			 				 [Date.UTC(2018, 10, 25, 0, 23, 0),40],
			 				 [Date.UTC(2018, 10, 25, 0, 24, 0),50],
			 				 [Date.UTC(2018, 10, 25, 0, 25, 0),80]]
			}]
		});

		Highcharts.chart('ram-graphic', {
			chart: {
				type: 'area'
			},
			title: {
				text: ''
			},
			subtitle: {
				text: 'Disponível x Cache x Utilizada nos últimos 25 minutos'
			},
			xAxis: {
				/*categories: ['08:00', '08:01', '08:02', '08:03', '08:04', '08:05', '08:06', '08:07', '08:08', '08:09', '08:10', '08:11', '08:12', '08:13'],
				tickmarkPlacement: 'on',*/
				type: 'datetime',
        dateTimeLabelFormats: { // don't display the dummy year
            month: '%e. %b',
            year: '%b'
        },
				title: {
					enabled: false
				}
			},
			yAxis: {
				title: {
					text: 'Megabits (mb)'
				},
				labels: {
					formatter: function() {
						return this.value;
					}
				}
			},
			tooltip: {
				split: true,
				valueSuffix: ' Megabits (mb)'
			},
			plotOptions: {
				area: {
					stacking: 'normal',
					lineColor: '#666666',
					lineWidth: 1,
					marker: {
						lineWidth: 1,
						lineColor: '#666666'
					}
				},
				series: {
					lineColor: '#fff'
				}
			},
			series: [{
				name: 'Disponível',
				color: '#9ED17F',
				marker: {
					symbol: "circle",
					radius: 0
				},
				data: [[Date.UTC(2018, 10, 25, 0, 0, 0),3300],
							 [Date.UTC(2018, 10, 25, 0, 1, 0),3000],
							 [Date.UTC(2018, 10, 25, 0, 2, 0),3500],
							 [Date.UTC(2018, 10, 25, 0, 3, 0),4000],
							 [Date.UTC(2018, 10, 25, 0, 4, 0),3300],
							 [Date.UTC(2018, 10, 25, 0, 5, 0),3000],
							 [Date.UTC(2018, 10, 25, 0, 6, 0),3500],
							 [Date.UTC(2018, 10, 25, 0, 7, 0),4000],
							 [Date.UTC(2018, 10, 25, 0, 8, 0),3300],
							 [Date.UTC(2018, 10, 25, 0, 9, 0),3000],
							 [Date.UTC(2018, 10, 25, 0, 10, 0),3500],
							 [Date.UTC(2018, 10, 25, 0, 11, 0),4000],
							 [Date.UTC(2018, 10, 25, 0, 12, 0),3300],
							 [Date.UTC(2018, 10, 25, 0, 13, 0),3000],
							 [Date.UTC(2018, 10, 25, 0, 14, 0),3300],
							 [Date.UTC(2018, 10, 25, 0, 15, 0),3000],
							 [Date.UTC(2018, 10, 25, 0, 16, 0),3500],
							 [Date.UTC(2018, 10, 25, 0, 17, 0),4000],
							 [Date.UTC(2018, 10, 25, 0, 18, 0),3300],
							 [Date.UTC(2018, 10, 25, 0, 19, 0),3000],
							 [Date.UTC(2018, 10, 25, 0, 20, 0),3500],
							 [Date.UTC(2018, 10, 25, 0, 21, 0),4000],
							 [Date.UTC(2018, 10, 25, 0, 22, 0),3300],
							 [Date.UTC(2018, 10, 25, 0, 23, 0),3000],
							 [Date.UTC(2018, 10, 25, 0, 24, 0),3500],
							 [Date.UTC(2018, 10, 25, 0, 25, 0),4000],
							 [Date.UTC(2018, 10, 25, 0, 26, 0),3300],
							 [Date.UTC(2018, 10, 25, 0, 27, 0),3000]]
			},

			{
				name: 'Cache',
				color: '#9fbff2',
				marker: {
					symbol: "circle",
					radius: 0
				},
				data: [[Date.UTC(2018, 10, 25, 0, 0, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 1, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 2, 0),500],
							 [Date.UTC(2018, 10, 25, 0, 3, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 4, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 5, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 6, 0),500],
							 [Date.UTC(2018, 10, 25, 0, 7, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 8, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 9, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 10, 0),500],
							 [Date.UTC(2018, 10, 25, 0, 11, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 12, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 13, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 14, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 15, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 16, 0),500],
							 [Date.UTC(2018, 10, 25, 0, 17, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 18, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 19, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 20, 0),500],
							 [Date.UTC(2018, 10, 25, 0, 21, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 22, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 23, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 24, 0),500],
							 [Date.UTC(2018, 10, 25, 0, 25, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 26, 0),200],
							 [Date.UTC(2018, 10, 25, 0, 27, 0),200]]
			},
			{
				name: 'Utilizada',
				color: '#f24848',
				marker: {
					symbol: "circle",
					radius: 0
				},
				data: [[Date.UTC(2018, 10, 25, 0, 0, 0),1500],
							 [Date.UTC(2018, 10, 25, 0, 1, 0),1800],
							 [Date.UTC(2018, 10, 25, 0, 2, 0),1000],
							 [Date.UTC(2018, 10, 25, 0, 3, 0),800],
							 [Date.UTC(2018, 10, 25, 0, 4, 0),1500],
							 [Date.UTC(2018, 10, 25, 0, 5, 0),1800],
							 [Date.UTC(2018, 10, 25, 0, 6, 0),1000],
							 [Date.UTC(2018, 10, 25, 0, 7, 0),800],
							 [Date.UTC(2018, 10, 25, 0, 8, 0),1500],
							 [Date.UTC(2018, 10, 25, 0, 9, 0),1800],
							 [Date.UTC(2018, 10, 25, 0, 10, 0),1000],
							 [Date.UTC(2018, 10, 25, 0, 11, 0),800],
							 [Date.UTC(2018, 10, 25, 0, 12, 0),1500],
							 [Date.UTC(2018, 10, 25, 0, 13, 0),1800],
							 [Date.UTC(2018, 10, 25, 0, 14, 0),1500],
							 [Date.UTC(2018, 10, 25, 0, 15, 0),1800],
							 [Date.UTC(2018, 10, 25, 0, 16, 0),1000],
							 [Date.UTC(2018, 10, 25, 0, 17, 0),800],
							 [Date.UTC(2018, 10, 25, 0, 18, 0),1500],
							 [Date.UTC(2018, 10, 25, 0, 19, 0),1800],
							 [Date.UTC(2018, 10, 25, 0, 20, 0),1000],
							 [Date.UTC(2018, 10, 25, 0, 21, 0),800],
							 [Date.UTC(2018, 10, 25, 0, 22, 0),1500],
							 [Date.UTC(2018, 10, 25, 0, 23, 0),1800],
							 [Date.UTC(2018, 10, 25, 0, 24, 0),1000],
							 [Date.UTC(2018, 10, 25, 0, 25, 0),800],
							 [Date.UTC(2018, 10, 25, 0, 26, 0),1500],
							 [Date.UTC(2018, 10, 25, 0, 27, 0),1800]]
			}]
		});


	</script>

	<?php
	include("footer.php");
	?>
